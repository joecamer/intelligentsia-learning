export const __scroll = (time = 1000) => {
    let y = window.scrollY
    const interval = time / 50
    const step = y / interval
    const go = setInterval(() => {
        if (y > 0) {
            window.scrollTo(0, y - step)
            y = window.scrollY
        } else {
            window.scrollTo(0,0)
        }
        // console.log(`y: ${y} - step: ${step} - int: ${interval}`)
    }, interval)

    setTimeout(() => {
        clearInterval(go)
    }, time)
}
