// VeeValidate
import {extend} from 'vee-validate';
import {required, min, max, regex, required_if, email, integer} from 'vee-validate/dist/rules';

extend('email', {
  ...email,
  message: 'The field {_field_} must be an email'
});
extend('required', {
  ...required,
  message: 'The field {_field_} is required'
});
extend('required_if', {
  ...required_if,
  message: 'The field {_field_} is required'
});

