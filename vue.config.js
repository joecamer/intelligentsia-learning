const path = require('path')
require('dotenv').config()
module.exports = {
  devServer: {
    proxy: process.env.PROXY || 'https://learning.upgradegroup.org'
  },

  configureWebpack: {
    resolve: {
      alias: {
        '@img': path.resolve(__dirname, 'src/assets/img/')
      }
    }
  },

  // output built static files to Laravel's public dir.
  // note the "build" script in package.json needs to be modified as well.
  outputDir: '../../../public/assets/home',
  chainWebpack: config => {
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
    types.forEach(type => addStyleResource(config.module.rule('scss').oneOf(type)))
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/assets/home/'
    : '/',

  // modify the location of the generated HTML file.
  indexPath: process.env.NODE_ENV === 'production'
    ? '../../../resources/views/home.blade.php'
    : 'index.html',

}

function addStyleResource(rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        path.resolve(__dirname, './src/assets/sass/_variables.scss'),
        path.resolve(__dirname, './src/assets/sass/_mixins.scss'),
      ],
    })
}
